<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SelectController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Employer\JobController;
use App\Http\Controllers\Auth\RegisterController;


Route::group(['middleware'=>"web"], function(){
    //employer routes
    Route::get('/', [HomeController::class, 'index'])->name('home');
 
    Route::get('login', [LoginController::class, 'index'])->name('login');
    Route::post('login', [LoginController::class, 'store']);

    Route::post('logout', [LogoutController::class, 'store'])->name('logout');

    Route::get('registerCandidate', [RegisterController::class, 'index']);

    Route::get('register', [RegisterController::class, 'index'])->name('register');
    Route::post('register', [RegisterController::class, 'store']);

    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    
    Route::post('postJobs', [JobController::class, 'store']);
    Route::get('postJobs', [JobController::class, 'index'])->name('postJobs');
    Route::get('/jobs/search', [JobController::class, 'search'])->name('jobs.search');

    Route::get('deleteJob/{id}',[JobController::class, 'deleteJob']);
    Route::get('editJob/{id}', [JobController::class, 'editJob']);

    Route::post('editJob', [JobController::class, 'updateJob'])->name('editJob');

});
    //candidate routes
    Route::post('registerCandidate', [RegisterController::class, 'storeCandidate'])->name('registerCandidate');

    Route::get('registerCandidate', [RegisterController::class, 'index']);  

    Route::get('loginCandidate', [LoginController::class, 'index'])->name('loginCandidate');
    Route::post('loginCandidate', [LoginController::class, 'storeCandidate']);

    Route::get('selectRegister', [SelectController::class, 'index'])->name('selectRegister');
    Route::get('selectLogin', [SelectController::class, 'index'])->name('selectLogin');

    Route::post('applyJob', [JobController::class, 'applyJob'])->name('applyJob');
    Route::delete('unapplyJob', [JobController::class, 'unapplyJob'])->name('unapplyJob');

    Route::post('logoutCandidate', [LogoutController::class, 'storeCandidate'])->name('logoutCandidate');

    Route::get('myJobs', [JobController::class, 'displayJobs']);



