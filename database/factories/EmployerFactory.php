<?php

namespace Database\Factories;

use App\Models\Employer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employer>
 */
class EmployerFactory extends Factory
{
    
    protected $model= Employer::class;
    
    public function definition()
    {
        
        return [
            'company_name' => $this->faker->company,
            'company_address' => $this->faker->address,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'gender' => $this->faker->randomElement(['male', 'female']),
            'contact' => $this->faker->phoneNumber,
            'location' => $this->faker->city,
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('password'), // Default password for all seeded employers  
        ];
    
    }
}
