<?php

namespace Database\Factories;

use App\Models\Employer;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Job>
 */
class JobFactory extends Factory
{
    
    public function definition(): array
    {

        $createdAt = $this->faker->dateTimeBetween('-30 days', 'now');
        $updatedAt = $this->faker->dateTimeBetween($createdAt, 'now');

        return [
            'job_title' => $this->faker->jobTitle,
            'description' => $this->faker->paragraph,
            'budget' => $this->faker->numberBetween(2,6),
            'job_type' => $this->faker->randomElement(['full', 'part']),
            'job_location' => $this->faker->randomElement(['mumbai', 'pune', 'banglore', 'delhi']),
            'experience_required' => $this->faker->numberBetween(0,6),
            'employer_id' => function(){
                return Employer::inRandomOrder()->first()->id;
            },
            'created_at' => $createdAt,
            'updated_at' => $updatedAt, 
        ];
    }
}
