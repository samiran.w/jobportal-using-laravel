<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('candidate_jobs', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('candidate_id')->unsigned();
          $table->integer('job_id')->unsigned();
          $table->foreign('candidate_id')->references('id')->on('candidates')->onDelete('cascade');
          $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('candidate_job');
    }
};
