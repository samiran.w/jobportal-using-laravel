<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Candidate extends Model implements Authenticatable
{
    use HasFactory, Notifiable;

    protected $guard = 'candidate';
    protected $table = 'candidates';

    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'dob',
        'location',
        'contact',
        'email',
        'password',
        'resume',
        'experience',
        'ctc',   
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'candidate_jobs')->withTimestamps();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthIdentifierName()
    {
        return 'id';
    }
}
