<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $path = $request->path();
        if (($path=="login" ||$path=="register") && (auth()->guard('employer')->check()))
        {
            return redirect()->route('home');
        } 
        else if (($path == "dashboard" || $path =="postJobs") && !auth()->guard('employer')->check())
        {
            return redirect()->route('selectLogin');
        }
        return $next($request);
    }
}
