<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware(['CustomAuth']);

    //     //$this->middleware(['auth']);

    // }

    public function index()
    {   
        // to check if jobs array is being returned
        //dd(auth()->guard('employer')->user()->jobs);
        return view('dashboard');
        
    }
}
