<?php

namespace App\Http\Controllers;

use App\Models\Job;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $pagination = config('app.pagination.default');

        $jobs = Job::with('employer')->orderBy('created_at', 'desc')->paginate($pagination);
        //dd($jobs);
        return view('home', [
            'jobs' => $jobs, 
        ]);

    }
}
