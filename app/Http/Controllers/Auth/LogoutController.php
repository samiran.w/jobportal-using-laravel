<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    public function store()
    {
        auth()->guard('employer')->logout();
        return redirect()->route('home');
    }

    public function storeCandidate()
    {
        auth()->guard('candidate')->logout();
        return redirect()->route('home');
    }
}
