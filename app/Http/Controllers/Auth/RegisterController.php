<?php

namespace App\Http\Controllers\Auth;

use App\Models\Employer;
use App\Models\Candidate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {

        $uri = request()->path();

        if ($uri === 'registerCandidate') {
            return view('auth.registerCandidate');
        } else if ($uri === 'register') {
            return view('auth.register');
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "company_name" => "required|max:225",
            "company_address" => "required|max:225",
            "first_name" => "required|max:225",
            "last_name" => "required|max:225",
            "gender" => "required",
            "contact" => "required",
            "location" => "required|max:225",
            "email" => "required|email|max:225",
            "password" => "required|confirmed"
        ]);

        //dd($request);
        Employer::create([
            "company_name" => $request->company_name,
            "company_address" => $request->company_address,
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "gender" => $request->gender,
            "contact" => $request->contact,
            "location" => $request->location,
            "email" => $request->email,
            "password" => Hash::make($request->password)
        ]);


        auth()->guard('employer')->attempt($request->only('email', 'password'));
        return redirect()->route('dashboard');
    }

    public function storeCandidate(Request $request)
    {

        //dd($request);
        $this->validate($request, [
            "first_name" => "required|max:225",
            "last_name" => "required|max:225",
            "gender" => "required",
            "dob" => "required|date_format:d-m-Y",
            "location" => "required|max:225",
            "contact" => "required",
            "email" => "required|email|max:225",
            "password" => "required|confirmed",
            "resume" => "required|mimes:pdf,docx|max:2048",
            "experience" => "required|numeric",
            "ctc" => "required|numeric"
        ]);

        Candidate::create([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "gender" => $request->gender,
            "dob" => date("Y-m-d",strtotime($request->dob)),
            "location" => $request->location,
            "contact" => $request->contact,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "resume" =>  $request->file('resume')->store('pdfs'),
            "experience" => $request->experience,
            "ctc" => $request->ctc,

        ]);

       //dd($request->hasFile('resume'));
        auth()->guard('candidate')->attempt($request->only('email', 'password'));

        dd(auth()->guard('candidate')->check());

        // Auth::guard('candidate')->attempt([
        //     'email' => $request->email,
        //     'password' => $request->password,
        // ]);

        // dd(Auth::guard('candidate')->check());
        
        return redirect()->route('dashboard');
    }
}
