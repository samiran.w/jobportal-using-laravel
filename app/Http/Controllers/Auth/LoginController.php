<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class LoginController extends Controller
{
    public function index()
    {   
        $uri = request()->path();

        if ($uri === 'loginCandidate') {
            return view('auth.loginCandidate');
        } else if ($uri === 'login') {
            return view('auth.login');
        }

    }

    public function store(Request $request)
    {

        $this->validate($request, [
            "email" => "required|email|max:255",
            "password" => "required"
        ]);

        if (auth()->guard('employer')->attempt($request->only('email', 'password'))) {
            return redirect()->route("home");
        } else {
            // Authentication failed
            return back()->with('status', 'Invalid login details');
        }

    }

    public function storeCandidate(Request $request)
    {

        $this->validate($request, [
            "email" => "required|email",
            "password" => "required"
        ]);


        if (auth()->guard('candidate')->attempt($request->only('email', 'password'))) {
            return redirect()->route("home");
        } else {
            // Authentication failed
            return back()->with('status', 'Invalid login details');
        }

    }
}
