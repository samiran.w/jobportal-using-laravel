<?php

namespace App\Http\Controllers\Employer;

use App\Models\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Candidate;

class JobController extends Controller
{
    public function index()
    {
        return view('employer.postJobs');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            "job_title" => "required|max:255",
            "description" => "required",
            "budget" => "required|numeric",
            "job_type" => "required",
            "job_location" => "required",
            "experience_required" => "required|numeric",
        ]);

        Job::Create([
            "job_title" => $request->job_title,
            "description" => $request->description,
            "budget" => $request->budget,
            "job_type" => $request->job_type,
            "job_location" => $request->job_location,
            "experience_required" => $request->experience_required,
            "employer_id" => auth()->guard('employer')->user()->id,
        ]);
        
        return redirect()->route('dashboard');
    }


    public function search(Request $request)
    {
        $query = $request->input('query');
        $pagination = config('app.pagination.default');

        $jobs = Job::with('employer')
        ->where('job_title', 'LIKE', '%'.$query.'%')
        ->orWhere('job_location', 'LIKE', '%'.$query.'%')
        ->orWhereHas('employer', function ($employerQuery) use ($query) {
            $employerQuery->where('company_name', 'LIKE', '%' . $query . '%');
        })
        ->orderBy('created_at', 'desc')
        ->paginate($pagination);

        return view('home', [
            'jobs' => $jobs,
        ]);
    }


    public function applyJob(Request $request)
    {
        $candidate = Candidate::find(auth()->guard('candidate')->user()->id);
        $job = $request->job_id;

        $candidate->jobs()->attach($job);

        return redirect()->route('home');
    }


    public function unapplyJob(Request $request)
    {
        $candidate = Candidate::find(auth()->guard('candidate')->user()->id);
        $job = $request->job_id;

        $candidate->jobs()->detach($job);

        return redirect()->route('home');
    }


    public function displayJobs()
    {
        $employerId = auth()->guard('employer')->id();

        $jobs = Job::withCount('candidate')
            ->where('employer_id', $employerId)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('employer.myJobs', [
            'jobs' => $jobs,
        ]);
    }


    public function deleteJob($id)
    {
        $job = Job::find($id);
        $job->delete();
        return redirect('myJobs');

    }


    public function editJob($id)
    {
        $job = Job::find($id);
        return view('employer.editJob',['job'=> $job]);
    }

    public function updateJob(Request $request)
    {

        $this->validate($request, [
            "job_title" => "required|max:255",
            "description" => "required",
            "budget" => "required|numeric",
            "job_type" => "required",
            "job_location" => "required",
            "experience_required" => "required|numeric",
        ]);
    
        $job = Job::findOrFail($request->id);
    
        $job->update([
            "job_title" => $request->job_title,
            "description" => $request->description,
            "budget" => $request->budget,
            "job_type" => $request->job_type,
            "job_location" => $request->job_location,
            "experience_required" => $request->experience_required,
        ]);
    
        return redirect()->route('employer.myJobs');
    }

}
