<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Candidate;
use Illuminate\Http\Request;

class SelectController extends Controller
{
    public function index()
    {
        $uri = request()->path();

        $jobCount = Job::count();
        $candidateCount = Candidate::count();
        
        if( $uri === 'selectRegister'){

            return view('selectRegister', [
                'jobCount' => $jobCount,
                'candidateCount' => $candidateCount,
            ]);

        }else if( $uri === 'selectLogin'){

            return view('selectlogin', [
                'jobCount' => $jobCount,
                'candidateCount' => $candidateCount,
            ]);

        }
        
    }
}
