@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-6/12 p-6">

            <form action="{{ route('jobs.search') }}" method="GET" class="mb-4">
               
                <div class="flex items-center justify-between mb-3">
                    <h1 class="text-4xl font-bold pl-4">Find Jobs</h1>
                    <div class="text-sm text-gray-700 pr-4 pt-5">
                        Showing {{ $jobs->firstItem() }} to {{ $jobs->lastItem() }} of {{ $jobs->total() }} entries
                    </div>  
                </div>

                <div class="flex flex-around">
                    <input type="text" name="query" placeholder="  Search jobs..." class="w-10/12 p-2 border rounded-lg">
                    <button type="submit" class="w-2/12 bg-sky-500 text-white p-4 rounded-lg ml-2 hover:bg-sky-700">Search</button>
                </div>

            </form>
            {{--            <div class="flex flex-col">--}}
            @if($jobs->count())
                @foreach($jobs as $job)
                    <div class="w-full mb-4 shadow hover:shadow-2xl bg-white p-6 rounded-lg relative">
                        
                        <a href="" class="font-bold">{{ $job->job_title }}</a> 
                        <span class="text-gray-500 absolute right-4 text-sm">{{ $job->created_at->diffForHumans() }}</span>
                
                        <p class="mb-2 text-gray-600">{{ $job->employer->company_name}} | {{ $job->employer->first_name." ".$job->employer->last_name }}</p>

                        <table>
                            <tr>
                                <td>
                                    <i class="fa-solid fa-location-dot" style="color: #3abdf8; padding-top: 5px;"></i>      
                                </td>
                                <td>
                                    <p class="pl-2">Locaton : <b> {{ $job->job_location }} </b></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa-solid fa-business-time" style="color: #38bdf8; padding-top: 5px;"></i>
                                </td>
                                <td>
                                    <p class="pl-2">Experience required : <b> {{ $job->experience_required }} Years </b></b> 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa-solid fa-house-laptop" style="color: #38bdf8; padding-top: 5px;"></i>
                                </td>
                                <td>
                                    <p class="pl-2">Job Type : <b>  {{ $job->job_type }} time </b></b>                                      
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i class="fa-solid fa-indian-rupee-sign" style="color: #38bdf8; padding-top: 5px;"></i>
                                </td>
                                <td>
                                    <p class="pl-2">Salary : <b> {{ $job->budget }} LPA </b></b>        
                                </td>
                            </tr>
                        </table>
                        
                        <div class="absolute bottom-4 right-4">
                            @if(auth()->guard('candidate')->check() && auth()->guard('candidate')->user()->jobs->contains($job))
                                <form method="post" action="{{ route('unapplyJob') }}">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    <button
                                        type="submit" class="bg-sky-200 text-gray-800 hover:shadow-lg hover:bg-sky-500 hover:text-white  py-3 px-4 rounded-lg">Unapply
                                    </button>
                                </form>
                            @elseif(auth()->guard('candidate')->check())
                                <form method="post" action="{{ route('applyJob') }}">
                                    @csrf
                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                    <button
                                        type="submit" class="bg-gray-100 text-gray-800 hover:shadow-lg hover:bg-sky-500 hover:text-white  py-3 px-4 rounded-lg">Easy Apply
                                    </button>
                                </form>
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
                <div class="mb4 font-bold">Sorry, there are no jobs here.</div>
            @endif

            <div class="mt-4">
                <div class="flex items-center justify-between">
                        {{ $jobs->links() }}
                </div>
            </div>

        </div>
            

        </div>
    </div>
    
@endsection

