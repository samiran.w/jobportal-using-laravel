@extends('layouts.app')

@section('content')
    <div class="flex justify-center items-center">
        <div class="w-8/12 flex mt-32 justify-evenly p-6 rounded-lg">
            <a href="{{ route('loginCandidate') }}" class="w-600 h-400 bg-white rounded-lg shadow-m py-4 px-20 flex flex-col items-center justify-center hover:shadow-2xl card">
                <div class="text-center">
                  <i class="fa-solid fa-person text-center icon transition-colors" style="color: #38bdf8; font-size: 180px; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);"></i>
                </div>
                <hr class="w-40 mt-4 border-t-2 border-gray-300">
                <h2 class="text-xl font-bold mt-4">Login as Candidate</h2>
                <p class="mt-2 text-white italic transition-colors template-text">Meet more than {{ $jobCount }}+ potential jobs waiting for you</p>
            </a>

            <a href="{{ route('login') }}" class="w-600 h-400 bg-white rounded-lg shadow-m py-4 px-20 flex flex-col items-center justify-center hover:shadow-2xl card">
                <div class="text-center">
                  <i class="fa-solid fa-people-group text-center icon transition-colors" style="color: #38bdf8; font-size: 180px; text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);"></i>
                </div>
                <hr class="w-40 mt-4 border-t-2 border-gray-300">
                <h2 class="text-xl font-bold mt-4">Login as Employer</h2>
                <p class="mt-2 text-white italic transition-colors template-text "> More than {{ $candidateCount }}+ candidates waiting to be explored</p>
            </a>
        </div>
        
    </div>
@endsection
