<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Job Portal</title>
    @vite('resources/css/app.css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>

</head>
<body class="bg-gray-200">
    <nav class="p-6 bg-sky-400 text-white flex justify-between mb-6">
        <ul class="flex items-center">
            <li>
                <a href="{{ route('home') }}" class="p-3">Home</a>
            </li>
            <li>
                <a href="{{ route('dashboard') }}" class="p-3">Dashboard</a>
            </li>
        </ul> 
        <ul class="flex items-center">

            @if(auth()->guard('employer')->check())
                <li>
                    <a href="" class="p-3">{{ auth()->guard('employer')->user()->first_name ?? "Anon Employee"}}</a>
                </li>
                <li>
                    <form action="{{ route('logout') }}" method="POST" class ="p-3 inline">
                        @csrf
                        <button type="submit">logout</button>
                    </form>     
                </li>
            @elseif(auth()->guard('candidate')->check())
                <li>
                    <a href="" class="p-3">{{ auth()->guard('candidate')->user()->first_name ?? "Anon Candidate"}}</a>
                </li>
                <li>
                    <form action="{{ route('logoutCandidate') }}" method="POST" class ="p-3 inline">
                        @csrf
                        <button type="submit">logout</button>
                    </form> 
                </li>
            @else 
                <li>
                    <a href="{{ route('selectLogin') }}" class="p-3">Login</a>
                </li>
                <li>
                    <a href="{{ route('selectRegister') }}" class="p-3">Register</a>
                </li>
            @endif
            
        </ul> 
    </nav>
    @yield('content')
    
    <script>

        //to make text appear on cards in selectRegester page
        $(document).ready(function() {
          $(".card").hover(function() {
            $(this).find(".template-text").css("color", "#38bdf8");
            $(this).find(".icon").css("color", "black");
          }, function() {
            $(this).find(".template-text").css("color", "white");
            $(this).find(".icon").css("color", "#38bdf8");
          });

        });
        </script>
</body>
</html>