@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-4/12 bg-white p-6 rounded-lg">
            <h1 class="mb-4 pl-4 text-2xl font-bold">Register as Employer</h1>

            <form action="{{ route('register') }}" method="post">
                @csrf

                @error('company_name')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="company_name" class="sr-only">Company Name</label>
                    <input type="text" name="company_name" id="company_name" placeholder="Company Name" value="{{ old('company_name')}}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('company_name') border-red-500 @enderror">
                </div>
    
                @error('company_address')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="company_address" class="sr-only">Company Address</label>
                    <textarea name="company_address" id="company_address" cols="30" rows="3"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('company_address') border-red-500 @enderror"
                    placeholder="Enter Company Address">{{ old('company_address')}}</textarea>
                </div>

                @error('first_name')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="first_name" class="sr-only">First Name</label>
                    <input type="text" name="first_name" id="first_name" placeholder="First Name" value="{{ old('first_name')}}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('first_name') border-red-500 @enderror">
                </div>
                
                @error('last_name')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">  
                    <label for="last-name" class="sr-only">Last Name</label>
                    <input type="text" name="last_name" id="last-name" placeholder="Last Name" value="{{ old('last_name')}}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('last_name') border-red-500 @enderror">
                </div>

                @error('gender')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <h3 class="mb-4 text-gray-500 pl-4 ">Select Gender :</h3>
                    <ul class="items-center w-full text-sm font-medium text-gray-900 bg-blue border border-gray-200 rounded-lg sm:flex @error('gender') border-red-500 @enderror">
                        <li class="w-full border-b border-gray-200 sm:border-b-0 sm:border-r">
                            <div class="flex items-center pl-3">
                                <input id="horizontal-list-radio-license" type="radio" value="male" {{ old('gender') == "male" ? 'checked' : '' }} name="gender" 
                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-sky-400">
                                <label for="horizontal-list-radio-license" class="w-full py-3 ml-2 text-sm font-medium text-gray-500">Male</label>
                            </div>
                        </li>
                        <li class="w-full border-b border-gray-200 sm:border-b-0 sm:border-r">
                            <div class="flex items-center pl-3">
                                <input id="horizontal-list-radio-id" type="radio" value="female" {{ old('gender') == "female" ? 'checked' : '' }} name="gender" 
                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-sky-400">
                                <label for="horizontal-list-radio-id" class="w-full py-3 ml-2 text-sm font-medium text-gray-500">Female</label>
                            </div>
                        </li>
                        <li class="w-full border-b border-gray-200 sm:border-b-0 sm:border-r">
                            <div class="flex items-center pl-3">
                                <input id="horizontal-list-radio-millitary" type="radio" value="other" {{ old('gender') == "other" ? 'checked' : '' }} name="gender" 
                                class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-sky-400">
                                <label for="horizontal-list-radio-millitary" class="w-full py-3 ml-2 text-sm font-medium text-gray-500">Other</label>
                            </div>
                        </li>
                    </ul>
                </div>

                @error('contact')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="contact" class="sr-only">Contact Number</label>
                    <input type="text" name="contact" id="contact" placeholder="Contact Number" value="{{ old('contact') }}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('contact') border-red-500 @enderror">
                </div>

                @error('location')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="location" class="sr-only">Location</label>
                    <input type="text" name="location" id="location" placeholder="Current Location" value="{{ old('location') }}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('location') border-red-500 @enderror">
                </div>

                @error('email')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="email" class="sr-only">Email</label>
                    <input type="text" name="email" id="email" placeholder="Enter Email" value="{{ old('email') }}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror">
                </div>

                @error('password')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">  
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" placeholder="Choose Password"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password') border-red-500 @enderror">
                </div>
                <div class="mb-4">  
                    <label for="password_confirmation" class="sr-only">Reenter Password</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Repeat Your Password"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg ">
                </div>

                <div>
                    <button type="submit" class="bg-sky-500 text-white px-4 py-3 rounded font-medium w-full">Register</button>
                </div>
            </form>
        </div>  
    </div>
@endsection()