@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-4/12 bg-white p-6 rounded-lg">
            <h1 class="mb-4 pl-4 text-2xl font-bold">Employer Login</h1>
            <form action="" method="post">
                @csrf

                @if(session("status"))
                    <div class="bg-red-500 p-4 rounded-lg mb-4 text-white text-center">
                        {{ session("status")}}
                    </div> 
                @endif

                @error('email')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">
                    <label for="email" class="sr-only">Email</label>
                    <input type="text" name="email" id="email" placeholder="Enter Email" value="{{ old('email') }}"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('email') border-red-500 @enderror">
                </div>

                @error('password')
                    <div class="text-red-500 mb-2 pl-4 text-sm">
                        {{ $message }}
                    </div>
                @enderror
                <div class="mb-4">  
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" placeholder="Choose Password"
                    class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('password') border-red-500 @enderror">
                </div>

                <div>
                    <button type="submweit" class="bg-sky-500 text-white py-3 rounded font-medium w-full">Login</button>
                </div>
            </form>
        </div>
    </div>
@endsection