@extends('layouts.app')

@section('content')
<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounded-lg">
        <h1 class="mb-4 pl-4 text-2xl font-bold">Post A Job</h1>
        <form action="{{ route('postJobs') }}" method="post">
            @csrf
            @error('job_title')
                <div class="text-red-500 mb-2 pl-4 text-sm">
                    {{ $message }}
                </div>
            @enderror
            <div class="mb-4">
                <label for="job_title" class="sr-only">Job Title</label>
                <input type="text" name="job_title" id="job_title" placeholder="Job Title" value="{{ old('job_title')}}"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('job_title') border-red-500 @enderror">
            </div>

            @error('description')
                <div class="text-red-500 mb-2 pl-4 text-sm">
                    {{ $message }}
                </div>
            @enderror
            <div class="mb-4">
                <label for="description" class="sr-only">Job Description</label>
                <textarea name="description" id="description" cols="30" rows="3"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('description') border-red-500 @enderror"
                placeholder="Job Description">{{ old('description')}}</textarea>
            </div>

            @error('budget')
                <div class="text-red-500 mb-2 pl-4 text-sm">
                    {{ $message }}
                </div>
            @enderror
            <div>
                <label for="budget" class="sr-only">Budget</label>
                <input type="text" name="budget" id="budget" placeholder="Budget( in lakhs )" value="{{ old('budget') }}"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('budget') border-red-500 @enderror">
            </div>


            @error('job_type')
            <div class="text-red-500 mb-2 pl-4 text-sm">
                {{ $message }}
            </div>
            @enderror
            <div class="mb-4">   
                <label for="job_type" class="pl-4 text-gray-500 sr-only">Job Type</label><br>
                <select name="job_type" id="job_type" class="bg-gray-100 border-2 w-full p-4 rounded-lg text-gray-500
                @error('job_type') border-red-500 @enderror">
                    <option disabled selected value>Select Job Type</option>
                    <option value="full">Full Time</option>
                    <option value="part">Part Time</option>
                </select
            </div>

            @error('job_location')
            <div class="text-red-500 mb-2 pl-4 text-sm">
                {{ $message }}
            </div>
            @enderror
            <div class="mb-4">   
                <label for="cars" class="pl-4 text-gray-500 sr-only">Job Location</label><br>

                <select name="job_location" id="job_location" class="bg-gray-100 border-2 w-full p-4 rounded-lg text-gray-500
                @error('job_location') border-red-500 @enderror">
                    <option disabled selected value>Select Job Location</option>
                    <option value="mumbai">Mumbai</option>
                    <option value="pune">Pune</option>
                    <option value="bangalore">Bangalore</option>
                    <option value="delhi">Delhi</option>
                </select
            </div>

            @error('experience_required')
            <div class="text-red-500 mb-2 pl-4 text-sm">
                {{ $message }}
            </div>
            @enderror
            <div class="mb-4">   
                <label for="experience_required" class="pl-4 text-gray-500 sr-only">Experience Required</label><br>
                <input type="text" name="experience_required" id="experience_required" placeholder="Experience in Years" value="{{ old('experience_required') }}"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('experience_required') border-red-500 @enderror">
            </div>

            
            
            <div class="mt-6">
                <button type="submit" class="bg-sky-500 text-white py-3 rounded font-medium w-full">Post</button>
            </div>
        </form>
    </div>
</div>
@endsection