@extends('layouts.app')

@section('content')
    <div class="flex justify-center">
        <div class="w-6/12 bg-white p-6 rounded-lg">
            <h1 class="mb-4 pl-4 text-2xl font-bold">My Jobs</h1>

@if($jobs->count())
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500 ">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 ">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Job Title
                </th>
                <th scope="col" class="px-6 py-3">
                    Job Location
                </th>
                <th scope="col" class="px-6 py-3">
                    Max CTC Budget
                </th>
                <th scope="col" class="px-6 py-3">
                    Applications
                </th>
                <th scope="col" class="px-6 py-3">
                    Actions
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($jobs as $job)
                <tr class="bg-white border-b ">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap ">
                        {{ $job->job_title }}
                    </th>
                    <td class="px-6 py-4 text-center">
                        {{ $job->job_location }}
                    </td>
                    <td class="px-6 py-4 text-center">
                        {{ $job->budget }}
                    </td>
                    <td class="px-6 py-4 text-center">
                        {{ $job->candidate_count }}
                    </td>
                    <td class="px-6 py-4 text-center">
                        <a href="{{ "editJob/".$job->id}}" class="font-medium text-blue-600  hover:underline">Edit</a> | 
                        <a href="{{ "deleteJob/".$job->id }}" class="font-medium text-blue-600  hover:underline">Delete</a>
                    </td>
                </tr> 
            @endforeach
        </tbody>
        @else
            <div class="mb4 font-bold">Sorry, it seems you've not posted any jobs yet</div>
        @endif
    </table>
</div>

          
    </div>
@endsection